import java.util.List;

import java.util.List;
import java.util.ArrayList;

public class Journee{
    private List<Integer> epreuve1;
    private List<Integer> epreuve2;

    public Journee(){
        this.epreuve1 = new ArrayList<>();
        this.epreuve2 = new ArrayList<>();

        for(int i = 0; i < 2; ++i){
            epreuve1.add(null);
            epreuve2.add(null);
        }
    }

    public Journee(Integer scoreApres, Integer scoreSoir, List<Integer> epreuve1){
        this.epreuve1 = epreuve1;
        this.epreuve1.add(scoreSoir, scoreApres);
    }

    public void ajouterScore(Integer scoreApresMidi, Integer scoreSoir){
        
    }

    public Integer sommeEpreuve(){
        Integer resultat = 0;
        for (Integer indice : this.epreuve1){
            resultat += indice;
        }
        return resultat;
    }

    public Integer toutesLesEpreuves(){
        return this.epreuve1.sommeEpreuve() + this.epreuve2.sommeEpreuve();
    }

}