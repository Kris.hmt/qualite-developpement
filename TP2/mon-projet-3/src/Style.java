import java.util.ArrayList;
import java.util.List;

public class Style {
    private Integer scoreApresMidi;
    private Integer scoreSoir;
    private List<Integer> liste;

    public Style(Integer score1, Integer score2){
        this.scoreApresMidi = score1;
        this.scoreSoir = score2;
        this.liste = new ArrayList<>();
    }

    public List<Integer> sommeScore(){
        return this.liste.add(this.scoreApresMidi + this.scoreSoir);
    }
}
